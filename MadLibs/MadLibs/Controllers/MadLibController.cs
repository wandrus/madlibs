﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MadLibs.Models;
using System.Net.Http;
using Newtonsoft.Json;


namespace MadLibs.Controllers
{
    public class MadLibController : AsyncController
    {
        //
        // GET: /MadLib/


        public ActionResult StoryService()
        { 
            return this.File("~/App_Data/StoriesDB.json","application/json");
        }

        public void IndexAsync()
        {
            String uri = Url.Action("StoryService", "MadLib", null, Request.Url.Scheme);
            HttpClient client = new HttpClient();
            var response = client.GetStringAsync(uri).Result;

            AsyncManager.Parameters["stories"] = JsonConvert.DeserializeObject<List<Story>>(response);            
        }

        public ActionResult IndexCompleted(List<Story> stories)
        {
            return View(stories);
        }
    }
}
