﻿$(function () {
    var Index;

    var paragraph = $('#Paragraph_' + Index);
    var articles = $('div[id^="Article_"]');
    var viewStoryBtn = $("#ViewStory");
    var partsOfSpeach = $('#POS');
    var stories = $('li[id^="Story_"]');
    var title = $('#StoryTitle');

    $("li").click(function () {
        var listItem = $(this);

        if (listItem.attr("id") != null) {
            var section = listItem.attr("id");

            if (section.substring(0, 5) === "Story") {
                //hide paragraphs to be on the safe side
                var allParagraphs = $('p[id^="Paragraph_"]').hide();
                $('#ErrorValidation').hide();

                ShowStory(section.replace("Story", "Article"));        
            }
            else if (section.substring(0, 8) === "Category") {
                //hide paragraphs to be on the safe side
                var allParagraphs = $('p[id^="Paragraph_"]').hide();
                $('#ErrorValidation').hide();

                var category = listItem.text();
                ShowCategory(category);
            }
            else {
            }
        }
    });

    function ShowStory(id) {
        var story = $("#" + id);

        if (story.is(":visible")) {
            articles.hide(500);
            viewStoryBtn.hide(500);
            partsOfSpeach.hide(500);
            title.text("");
            Index = null;
        }
        else {
            articles.hide(500);
            story.show(500);
            viewStoryBtn.show(500);
            partsOfSpeach.show(500);
            title.text(story.text());
            Index = story.attr("id").replace("Article_", "");
        }
    }

    function ShowCategory(category)
    {
        var category = $("." + category.replace(" ",""));

        if (category.is(":visible")) {
            //hide all
            stories.hide(500);
            articles.hide(500);
            viewStoryBtn.hide(500);
            partsOfSpeach.hide(500);
        }
        else {
            //hide all
            stories.hide(500);
            articles.hide(500);
            //show specific category
            category.show(500);
        }
    }

    viewStoryBtn.click(function () {

        var valid = true;

        var paragraph = $('#Paragraph_' + Index);

        var articleIndex = $('#Article_' + Index);

        //count parts of speech needed for story
        var nounCount = articleIndex.find('input[id^="Noun"]').length;
        var personCount = articleIndex.find('input[id^="Person"]').length;
        var placeCount = articleIndex.find('input[id^="Place"]').length;
        var thingCount = articleIndex.find('input[id^="Thing"]').length;
        var animalCount = articleIndex.find('input[id^="Animal"]').length;
        var verbCount = articleIndex.find('input[id^="Verb"]').length;
        var adverbCount = articleIndex.find('input[id^="Adverb"]').length;
        var adjectiveCount = articleIndex.find('input[id^="Adjective"]').length;
        var pasttenseverbCount = articleIndex.find('input[id^="PastTenseVerb"]').length;
 

        /* Check that all fields are filled */

        for (var i = 0; i < nounCount; i++) {
            var val = $('#NounInput_' + i.toString() + "_" + Index.toString()).val();
            if (val == null || val === '') {
                valid = false;
                break;
            }
        }

        for (var i = 0; i < personCount; i++) {
            var val = $('#PersonInput_' + i.toString() + "_" + Index.toString()).val();
            if (val == null || val === '') {
                valid = false;
                break;
            }
        }

        for (var i = 0; i < placeCount; i++) {
            var val = $('#PlaceInput_' + i.toString() + "_" + Index.toString()).val();
            if (val == null || val === '') {
                valid = false;
                break;
            }
        }

        for (var i = 0; i < thingCount; i++) {
            var val = $('#ThingInput_' + i.toString() + "_" + Index.toString()).val();
            if (val == null || val === '') {
                valid = false;
                break;
            }
        }

        for (var i = 0; i < animalCount; i++) {
            var val = $('#AnimalInput_' + i.toString() + "_" + Index.toString()).val();
            if (val == null || val === '') {
                valid = false;
                break;
            }
        }

        if (valid) {
            for (var i = 0; i < verbCount; i++) {
                var val = $('#VerbInput_' + i.toString() + "_" + Index.toString()).val();
                if (val == null || val === '') {
                    valid = false;
                    break;
                }
            }
        }

        if (valid) {
            for (var i = 0; i < pasttenseverbCount; i++) {
                var val = $('#PastTenseVerbInput_' + i.toString() + "_" + Index.toString()).val();
                if (val == null || val === '') {
                    valid = false;
                    break;
                }
            }
        }

        if (valid) {
            for (var i = 0; i < adverbCount; i++) {
                var val = $('#AdverbInput_' + i.toString() + "_" + Index.toString()).val();
                if (val == null || val === '') {
                    valid = false;
                    break;
                }
            }
        }

        if (valid) {
            for (var i = 0; i < adjectiveCount; i++) {
                var val = $('#AdjectiveInput_' + i.toString() + "_" + Index.toString()).val();
                if (val == null || val === '') {
                    valid = false;
                    break;
                }
            }
        }


        /* if still valid subsitute words into paragraph*/
        if (valid) {

            for (var i = 0; i < nounCount; i++) {
                var regex = new RegExp("#noun" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#NounInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            for (var i = 0; i < personCount; i++) {
                var regex = new RegExp("#person" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#PersonInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            for (var i = 0; i < placeCount; i++) {
                var regex = new RegExp("#place" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#PlaceInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            for (var i = 0; i < thingCount; i++) {
                var regex = new RegExp("#thing" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#ThingInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            for (var i = 0; i <animalCount; i++) {
                var regex = new RegExp("#animal" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#AnimalInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            for (var i = 0; i < verbCount; i++) {
                var regex = new RegExp("#verb" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#VerbInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            for (var i = 0; i < pasttenseverbCount; i++) {
                var regex = new RegExp("#pasttenseverb" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#PastTenseVerbInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            for (var i = 0; i < adverbCount; i++) {
                var regex = new RegExp("#adverb" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#AdverbInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            for (var i = 0; i < adjectiveCount; i++) {
                var regex = new RegExp("#adjective" + i.toString() + "#", "g");
                paragraph.text(paragraph.text().replace(regex, $('#AdjectiveInput_' + i.toString() + "_" + Index.toString()).val()));
            }

            $('#ErrorValidation').hide();
            paragraph.show(500);
        }
        else {
            //alert('Please fill in all fields. Thanks.');
            $('#ErrorValidation').show();
        }
    });
});