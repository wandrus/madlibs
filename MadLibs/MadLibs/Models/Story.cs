﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MadLibs.Models
{
    public class Story
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public int MissingNouns {get; set;}
        public int MissingNounPersons { get; set; }
        public int MissingNounPlaces { get; set; }
        public int MissingNounThings { get; set; }
        public int MissingNounAnimals { get; set; }
        public int MissingVerbs {get; set;}
        public int MissingAdjectives {get; set;}
        public int MissingAdverbs {get; set;}
        public int MissingPastTenseVerbs { get; set; }
    }

    public enum PartOfSpeach
    {
        Pronoun, //Descriptive Noun (eg: Tower of Pisa)
        Noun, //A person, place, thing or idea (eg: teacher, school, pen, hope, time, freedom)
        Adjective,//describes a noun or pronoun (eg: healthy, good, grumpy, sour, crabbed, beautiful)
        Adverb,//describes a verb, adjective or adverb (hint: many adv end with ly) (eg: easily, friendly, hopefully, stupidly, lovingly, happily)
        Verb,//shows action existence, or state of being (eg: can, have, are, were, shift, stop, halt, brake, is, stare, gawk, peek, watch)
        PastTenseVerb//shows action existed, or state that was (eg: had, shifted, stopped, halted, braked, was, stared, gawked, peeked, watched) 
    }
}